import React from 'react';
import { makeStyles } from "@material-ui/core/styles";
import Autocomplete from "@material-ui/lab/Autocomplete";
import CircularProgress from "@material-ui/core/CircularProgress";
import TextField from "@material-ui/core/TextField";
import finnhubClient from '../../utils/finnhubClient';

type SymbolProps = {
  symbol: string | null;
  setSymbol: (val: string | null) => void;
};

const useStyles = makeStyles(theme => ({
  inputRoot: {
    color: "white",
    "& .MuiOutlinedInput-notchedOutline": {
      borderColor: "white"
    },
    "&:hover .MuiOutlinedInput-notchedOutline": {
      borderColor: "white"
    },
    "&.Mui-focused .MuiOutlinedInput-notchedOutline": {
      borderColor: "#76B8E8"
    }
  },
  input: {
    color: "white"
  }
}));

const Symbol: React.FC<SymbolProps> = ({ symbol, setSymbol }: SymbolProps) => {
  const [open, setOpen] = React.useState(false);
  const [companiesList, setCompaniesList] = React.useState<string[]>([]);
  const classes = useStyles();

  const loading = open && companiesList.length === 0;

  React.useEffect(() => {

    if (!loading) {
      return undefined;
    }

    (() => {
      finnhubClient.stockSymbols('US', (error: Error, data: any) => {
        if (error) throw error;
        const mappedData = data
          .map(({symbol }: { symbol: string }) => (symbol))
        setCompaniesList(mappedData);
      });
    })();
  }, [loading]);

  const handleChange = (event: React.ChangeEvent<{}>, value:string | null) => {
    setSymbol(value);
  }

  return (
    <Autocomplete
      id="asynchronous-demo"
      style={{ width: 300, border: '1px #000' }}
      open={open}
      className={classes.inputRoot}
      onOpen={() => {
        setOpen(true);
      }}
      onClose={() => {
        setOpen(false);
      }}
      value={symbol} 
      onChange={handleChange}
      getOptionSelected={(option, value) => option === value}
      getOptionLabel={(option) => option}
      options={companiesList}
      loading={loading}
      renderInput={(params) => (
        <div className="symbol-container">
          <span className="filters-input">Symbol:</span>
          <TextField
            {...params}
            variant="outlined"
            InputProps={{
              ...params.InputProps,
              className: classes.input,
              endAdornment: (
                <React.Fragment>
                  {loading ? <CircularProgress color="inherit" size={20} /> : null}
                  {params.InputProps.endAdornment}
                </React.Fragment>
              ),
           }}
          />
        </div>
      )}
    />
  );
};

export default Symbol;
