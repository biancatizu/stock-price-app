import React from 'react';
import DateFilter from './Date';
import Submit from './Submit';
import Symbol from './Symbol';
import { StockCandles } from '../stock-chart';

type FiltersProps = {
  setStockCandles: React.Dispatch<React.SetStateAction<StockCandles>>;
  setHideChart: React.Dispatch<React.SetStateAction<boolean>>;
};

const Filters: React.FC<FiltersProps> = ({ setStockCandles, setHideChart }) => {
  const [startDate, setStartDate] = React.useState(new Date().toISOString().split('T')[0]);
  const [endDate, setEndDate] = React.useState(new Date().toISOString().split('T')[0]);
  const [symbol, setSymbol] = React.useState<string | null>('');

  return (
    <div className="filters-container">
      <DateFilter
        startDate={startDate}
        setStartDate={setStartDate}
        endDate={endDate}
        setEndDate={setEndDate}
      />
      <Symbol symbol={symbol} setSymbol={setSymbol} />
      <Submit
        setStockCandles={setStockCandles}
        startDate={startDate}
        endDate={endDate}
        symbol={symbol}
        setHideChart={setHideChart}
      />
    </div>
  );
};

export default Filters;
