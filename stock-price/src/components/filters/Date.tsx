import React, { useState } from 'react';

type DateProps = {
  startDate: string;
  setStartDate: (val: string) => void;
  endDate: string;
  setEndDate: (val: string) => void;
};

const DateFilter: React.FC<DateProps> = ({
  startDate,
  setStartDate,
  endDate,
  setEndDate,
}: DateProps) => {
  const [warning, setWarning] = useState(false);

  React.useEffect(() => {
    validateGivenDates();
  }, [startDate, endDate]);

  const handleStartDate = (event: React.ChangeEvent<HTMLInputElement>) => {
    setStartDate(event.target.value);
  };

  const handleEndDate = (event: React.ChangeEvent<HTMLInputElement>) => {
    setEndDate(event.target.value);
  };

  const validateGivenDates = () => {
    if (Date.parse(startDate) > Date.parse(endDate)) {
      setWarning(true);
    } else {
      setWarning(false);
    }
  };

  return (
    <div className="date-container">
      <label className="filters-label">
        Start date:
        <input
          className="filters-input"
          type="date"
          value={startDate}
          max={endDate}
          onChange={handleStartDate}
        />
      </label>
      <label className="filters-label">
        End date:
        <input
          className="filters-input"
          type="date"
          value={endDate}
          max={new Date().toISOString().split('T')[0]}
          onChange={handleEndDate}
        />
      </label>
      {warning && (
        <span
          style={{
            margin: '0 30px',
            color: '#aa2123',
            display: 'flex',
            flexDirection: 'row-reverse',
            alignItems: 'center',
          }}
        >
          Invalid data!
        </span>
      )}
    </div>
  );
};

export default DateFilter;
