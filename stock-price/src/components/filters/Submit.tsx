import React from 'react';
import { StockCandles } from '../stock-chart';
import { Button, withStyles } from '@material-ui/core';
import finnhubClient from '../../utils/finnhubClient';

const StyledButton = withStyles({
  root: {
    border: '1px solid #fff',
    color: 'white',
    height: 35,
    padding: '0 15px',
    margin: '10px',
    maxWidth: '180px',
  },
})(Button);

type SubmitProps = {
  startDate: string;
  endDate: string;
  symbol: string | null;
  setStockCandles: React.Dispatch<React.SetStateAction<StockCandles>>;
  setHideChart: React.Dispatch<React.SetStateAction<boolean>>;
};

const Submit: React.FC<SubmitProps> = ({
  startDate,
  endDate,
  symbol,
  setStockCandles,
  setHideChart,
}: SubmitProps) => {
  /** API accepts the date as a UNIX timestamp. I created this function
   * to format the date to call the API properly.
   */
  const getFormattedDate = (date: string): number => {
    return new Date(date).getTime() / 1000;
  };

  const handleSubmit = () => {
    finnhubClient.stockCandles(
      symbol?.toUpperCase(),
      'D',
      getFormattedDate(startDate),
      getFormattedDate(endDate),
      {},
      (error: Error, data: any, response: any) => {
        if (error) throw error;
        setStockCandles(data);
        if (response.status === 200) {
          setHideChart(false);
        }
      }
    );
  };

  return (
    <StyledButton variant="outlined" onClick={handleSubmit}>
      Submit
    </StyledButton>
  );
};

export default Submit;
