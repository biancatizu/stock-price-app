const candleStickChartOptions = {
    chart: {
      type: 'line',
    },
    colors:[ '#76B8E8','#FBC968'],
    title: {
      text: 'Stock Chart',
      align: 'left'
    },
    stroke: {
      width: [3, 1],
      dashArray: [0, 8, 5]
    },
    xaxis: {
      type: 'datetime'
    },
    yaxis: {
      tooltip: {
        enabled: true
      }
    } 
  };
  export { candleStickChartOptions};