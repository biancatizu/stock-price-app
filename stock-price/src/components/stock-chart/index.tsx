import React from 'react';
import Chart from 'react-apexcharts';
import {candleStickChartOptions} from './chartOptions';

export type StockCandles = { o: number[]; h: number[]; l: number[]; c: number[]; t: number[]; s?: string };

export type ChartProps = {
  stockCandles: StockCandles;
};

const StockChart: React.FC<ChartProps> = ({ stockCandles }) => {
  const [series, setSeries] = React.useState<any[]>([]);
  const [graphSize, setGraphSize] = React.useState({width: window.innerWidth - 100, height: window.innerHeight - 350});

  React.useEffect(() => {
    initializeChartData();
    window.onresize = adjustGraphSize;
  }, [stockCandles]);

  const initializeChartData = (): void => {
    if (stockCandles.s !== 'no_data') {
      const chartData = stockCandles.t.map((timestamp, index) => {
        return {
          x: getDate(timestamp),
          y: [
            stockCandles.o[index],
            stockCandles.h[index],
            stockCandles.l[index],
            stockCandles.c[index],
          ],
        };
      });
      const averageChartData= getAverageChartData();
      setSeries([{name:'stocks',type: 'candlestick', data: chartData}, {name: 'average',type: 'line', data: averageChartData}]);
    }
  };

  const getDate = (timestamp: number): Date => {
    return new Date(timestamp * 1000);
  };

  const getAverageChartData = (): any[] => {
    const average = stockCandles.c.reduce((a,b) => (a+b)) /stockCandles.c.length;
    return stockCandles.t.map(timestamp => ({x:getDate(timestamp), y:average}));
  }

  const adjustGraphSize = () => {
    const updatedSize = {width: window.innerWidth - 100, height: window.innerHeight - 350};
    setGraphSize(updatedSize);
  }

  return series.length ? (
    <div>
    <div className="chart-container">
    <Chart options={candleStickChartOptions} series={series} height={graphSize.height} width={graphSize.width}/>
  </div></div>) 
  : 
  <span style={{
    margin: '0 30px',
    color: '#aa2123',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
  }}>No data!</span>;
};

export default StockChart;
