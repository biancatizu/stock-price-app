import React from 'react';
import './App.css';
import StockChart, {StockCandles} from './components/stock-chart';
import Filters from './components/filters';
import Title from './components/Title';


function App() {
  const [stockCandles, setStockCandles] = React.useState<StockCandles>({o: [],
                                                          h: [],
                                                          l: [],
                                                          c: [],
                                                          t: []});
  const [hideChart, setHideChart] = React.useState(true);

  return (
    <div className="App">
      <Title />
      <Filters setStockCandles={setStockCandles} setHideChart={setHideChart} />
      {!hideChart && <StockChart stockCandles={stockCandles} />}
    </div>
  );
}

export default App;
