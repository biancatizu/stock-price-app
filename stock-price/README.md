<h1 align="center">Stock price</h1>

<p>Stock Price is a Web app which display the stock price filtered by a symbol and a period of time. Also, the user has the opportunity to overlay the average value of the stock prices on the chart.</p>

<h3 align="center">How to use it?</h3>
<p>In the homepage, insert a stock symbol or search a company listed on the stocks market and select a period of time.</p>

![](src/assets/homepage.png)

<p>After you submit those, you will see the price of the stock plotted on a chart. If you want, you can overlay the average value of the stock prices on the chart.</p>

![](src/assets/candle-sticks.png)

<h3 align="center">Project setup</h3>
<ul>
    <li>npm install</li>
    <li>npm start</li>
</ul>

<h3 align="center">Technologies</h3>
<p>Project is created with <b>React</b> and <b>TypeScript</b> and integrates Finnhub API with react-apexcharts.</p>

<h3 align="center">Inspiration</h3>
<p>Project substitute the exercise for an interview.</p>